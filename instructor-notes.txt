
- I aksed to know which git commit was used to generate your results.  Since
  you only have a few commits, this wasn't an issue, but I recommend in the
  future breaking up your changes into smaller commits.  This makes it easier
  to go back later, understand why you made the changes you did, identify
  sources of error, and reintegrate changes in different contexts.
- Your data points are plotted at non-integer locations?  I'm not sure how
  that happened.
- Valgrind isn't a performance profiler, so I think you mean just gprof.
  
