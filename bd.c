#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <string.h>
#include <math.h>
#include <omp.h>
#include <assert.h>
#include "bd.h"

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p)
{
  double f = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;
  double dx,dy,dz;
  double op[3*npos];

  for (int step=0; step<INTERVAL_LEN; step++)
  {
    int retval;
    int numpairs = 0;

      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
   
   memcpy(op,pos,npos*3*sizeof(double));
   int p;
   #pragma omp for schedule(guided)
   for (p = 0; p < numpairs; p++) {//Here,forces are intantiated at each interation
	const double krepul = 100; //And op is old positions
	double s = sqrt(dist2[p]);
	double force = krepul*(2-s);
	int i1 = pairs[2*p];
	int i2 = pairs[2*p+1];
	const double* ri = &op[3*i1];
	const double* rj = &op[3*i2];
	dx = ri[0]-rj[0];
	dy = ri[1]-rj[1];
	dz = ri[2]-rj[2];
	#pragma omp atomic
	pos[3*i1] += force*dx/s*DELTAT;
	#pragma omp atomic
	pos[3*i1+1] += force*dy/s*DELTAT;
	#pragma omp atomic
	pos[3*i1+2] += force*dz/s*DELTAT;
	#pragma omp atomic
	pos[3*i2] -= force*dx/s*DELTAT;
	#pragma omp atomic
	pos[3*i2+1] -= force*dy/s*DELTAT;
	#pragma omp atomic
	pos[3*i2+2] -= force*dz/s*DELTAT;
		
	}
	    

	int i;
   /* Although you allocated random number generators for different threads,
    * they are all using the same generator here, which I think is why you are
    * getting the wrong diffusion coefficient */
      #pragma omp for 
      for (i=0; i<3*npos; i++)
      {
       double noise = cse6230nrand(nrand);
       pos[i] += f*noise ;
    } 
 }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}
